//-----------------------------------------------------------------------------
//common settings
//version will be calculated using git repository
lazy val commonSettings = Seq(
  name                 := "boom"
  , version            := "0.1"
  , scalaVersion       := "2.12.10"
  , description        := "Big data on our m stars"
  , organization       := "IAA-CSIC"
  , homepage           := Some(url("http://www.iaa.csic.es"))
)
//-----------------------------------------------------------------------------
//Main class
lazy val mainClassName = "com.boom.Main"
//-----------------------------------------------------------------------------
//https://github.com/benblack86/finest
//To help improve code quality the following compiler flags will provide errors for deprecated and lint issues
scalacOptions ++= Seq(
  "-Xlint",
  "-deprecation",
  "-feature",
  "-unchecked"
  //  "-Xfatal-warnings"   //generate an error when warning raises. Enable when stable version was published
)
//-----------------------------------------------------------------------------
//dependencies versions
val apacheCommonsCsvVersion  = "1.9.0"
val apacheCommonsIO_Version  = "2.11.0"
val apacheLoggingVersion     = "2.17.2"
val apacheMathVersion        = "3.6.1"
val logbackClassicVersion    = "1.2.11"
val scallopVersion           = "4.1.0"
val scalaReflectVersion      = "2.12.10"
val scalaTestVersion         = "3.3.0-SNAP3"
val typeSafeVersion          = "1.4.2"
//-----------------------------------------------------------------------------
//assembly
mainClass in assembly := Some(mainClassName)
logLevel in assembly := Level.Warn

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", _*) => MergeStrategy.discard
  case _ => MergeStrategy.last
}
//-----------------------------------------------------------------------------
//tests
parallelExecution in test := false

//-----------------------------------------------------------------------------
//resolvers
//sbt-plugin
resolvers += Resolver.sonatypeRepo("public")
//-----------------------------------------------------------------------------
//external source code
val externalRootDirectory="/home/rafa/proyecto/common_scala/"

unmanagedSourceDirectories in Compile += file(externalRootDirectory + "configuration")
unmanagedSourceDirectories in Compile += file(externalRootDirectory + "dataType")
unmanagedSourceDirectories in Compile += file(externalRootDirectory + "geometry/distance")
unmanagedSourceDirectories in Compile += file(externalRootDirectory + "hardware/cpu")
unmanagedSourceDirectories in Compile += file(externalRootDirectory + "logger")
unmanagedSourceDirectories in Compile += file(externalRootDirectory + "util/file")
unmanagedSourceDirectories in Compile += file(externalRootDirectory + "util/path")
unmanagedSourceDirectories in Compile += file(externalRootDirectory + "util/pattern")
unmanagedSourceDirectories in Compile += file(externalRootDirectory + "util/time")
unmanagedSourceDirectories in Compile += file(externalRootDirectory + "util/parallelTask")
unmanagedSourceDirectories in Compile += file(externalRootDirectory + "util/util")

excludeFilter in (Compile, unmanagedSources) := {
  new SimpleFileFilter(f=> {
    val p = f.getCanonicalPath
      p.startsWith(externalRootDirectory + "dataType/conversion/") ||
      p.startsWith(externalRootDirectory + "dataType/pixelDataType/") ||
      p.startsWith(externalRootDirectory + "dataType/queue/") ||
      p.startsWith(externalRootDirectory + "dataType/stack/") ||
      p.startsWith(externalRootDirectory + "dataType/tree/") ||
      p.startsWith(externalRootDirectory + "logger/visual/") ||
      p.startsWith(externalRootDirectory + "image/jFits/")   ||
      p.startsWith(externalRootDirectory + "image/sourceDetection/")
  })}
//-----------------------------------------------------------------------------
//dependencies list
lazy val dependenceList = Seq(

  //manage configuration files. https://github.com/typesafehub/config
  "com.typesafe" % "config" % typeSafeVersion

  //logging: https://logging.apache.org/log4j/scala/index.html
  , "org.apache.logging.log4j" % "log4j-api" % apacheLoggingVersion
  , "org.apache.logging.log4j" % "log4j-core" % apacheLoggingVersion
  , "ch.qos.logback" % "logback-classic" % logbackClassicVersion

  // https://mvnrepository.com/artifact/commons-io/commons-io
  , "commons-io" % "commons-io" % apacheCommonsIO_Version

  //scala reflect
  , "org.scala-lang" % "scala-reflect" % scalaReflectVersion

  //command line parser : https://github.com/scallop/scallop
  , "org.rogach" %% "scallop" % scallopVersion

  //csv management: https://mvnrepository.com/artifact/org.apache.commons/commons-csv
  , "org.apache.commons" % "commons-csv" % apacheCommonsCsvVersion

  //apache math: https://commons.apache.org/proper/commons-math/
  , "org.apache.commons" % "commons-math3" % apacheMathVersion

  //scala test
  , "org.scalatest" %% "scalatest" % scalaTestVersion % Test
)
//=============================================================================
//remove duplicates libraries
libraryDependencies ~= { _.map(_.exclude("org.apache.logging.log4j", "log4j-slf4j-impl")) }
//=============================================================================
//root project
lazy val root = (project in file("."))
  .settings(commonSettings: _*)
  .enablePlugins(AssemblyPlugin)
  .settings(libraryDependencies ++= dependenceList)
//-----------------------------------------------------------------------------
//End of file 'build.sbt'
//-----------------------------------------------------------------------------
