/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/dic/2018
 * Time:  01h:38m
 * Description: https://github.com/scallop/scallop
 */
//=============================================================================
package com.boom.commandLine
//=============================================================================
import com.boom.Main._
import com.common.logger.VisualLazyLogger
import com.common.util.file.MyFile
import com.common.util.time.Time
import org.rogach.scallop._
//=============================================================================
import com.common.util.file.MyFile._
import com.common.util.path.Path._
//=============================================================================
//=============================================================================
object CommandLineParser {
  //---------------------------------------------------------------------------
  final val COMMAND_QUERY                      = "query"
  //---------------------------------------------------------------------------
  final val VALID_COMMAND_SEQ = Seq(
      COMMAND_QUERY
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
import CommandLineParser._
class CommandLineParser(args: Array[String]) extends ScallopConf(args) with VisualLazyLogger {
  version(s"""Moose2 (a.k.a m2)
             |\tVersion  : $VERSION_AND_DATE
             |\tAuthor   : $AUTHOR
             |\tFiliation: $FILIATION
             |\tLicense  : $LICENSE\n""".stripMargin)

  banner(s"""m2 syntax
            |m2 [configurationFile][command][parameters]
            |[commnads and parameters]
            |\t[$COMMAND_QUERY]
            | #---------------------------------------------------------------------------------------------------------
            | Example 1: Find but not save the images contained in the mongo database "astrometry" that includes the asteroid 'n'
            | 'n' can be the final or temporal designation.
            |
            |
            |  java -jar m2.jar --query 'n'
            | #---------------------------------------------------------------------------------------------------------
            | #---------------------------------------------------------------------------------------------------------
            |Options:
            |""".stripMargin)
  footer("\nFor detailed information, please consult the documentation!")
  //---------------------------------------------------------------------------
  val configurationFile = opt[String](default = Some("input/configuration/main.conf")
    ,  short = 'c'
    ,  descr = "m2 configuration file\n")

  val query = opt[String](required = false
    , noshort = true
    , descr = "Find all images that contains an asteroid\n")

  //---------------------------------------------------------------------------
  validate (configurationFile) { f =>
    if (!fileExist(f) && (!directoryExist(f))) Left(s"Invalid user argument 'configurationFile'. The configuration file '$f' does not exist")
    else Right(Unit)
  }
  //---------------------------------------------------------------------------
  validate(query) { s =>
     Right(Unit)
  }
  //---------------------------------------------------------------------------
  mutuallyExclusive(query)
  requireOne       (query)
  verify()
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CommandLineParser.scala
//=============================================================================
