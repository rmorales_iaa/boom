/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  15/Feb/2020
 * Time:  15h:57m
 * Description: None
 */
//=============================================================================
package com.boom
//=============================================================================
import com.boom.star.Star
import com.boom.star.distance.MyEuclideanDistance
import com.common.logger.VisualLazyLogger
import com.common.util.path.Path
import com.common.util.util.Util
//=============================================================================
//=============================================================================
object SandBox extends  VisualLazyLogger {
  //---------------------------------------------------------------------------
  def nonRepeatingComb[T](elems: List[T], genSize: Int, f: List[T] => Unit): Unit = {
    def nonRepeatingCombRec(elems: List[T], depth: Int, partResult: List[T]): Unit = {
      if (elems.size == depth) f(elems.reverse ::: partResult)
      else {
        if (!elems.isEmpty) {
          nonRepeatingCombRec(elems.tail, depth, partResult)
          if (depth > 0) nonRepeatingCombRec(elems.tail, depth - 1, elems.head :: partResult)
        }
      }
    }

    if (genSize < 0) throw new IllegalArgumentException("Negative generation sizes not allowed in nonRepeatingComb...")
    if (genSize > elems.size) throw new IllegalArgumentException("Generation sizes over elems.size not allowed in nonRepeatingComb...")
    nonRepeatingCombRec(elems.reverse, genSize, Nil)
  }

  def getCombinationSeq[A](ls: List[A], n: Int): List[List[A]] =
    if (n == 0) List(Nil)
    else Util.flatMapSubLists(ls) { sl =>
      getCombinationSeq(sl.tail, n - 1) map {
        sl.head :: _
      }
    }
  //---------------------------------------------------------------------------
  def getDistance() = {

    val inputCsvFile = "/home/rafa/Downloads/work_on_users/matilde/boom/boom_normalized.txt"
    val outputAttributeIndex = 3
    val maxResultDistanceCount = 10

    val oDirBase = Path.resetDirectory("/home/rafa/Downloads/work_on_users/matilde/boom/distance")
    val maxCombinationSize = Star.fieldIndexSeq.length
    for(combinationSize<-1 to  maxCombinationSize) {
      val oDir = Path.resetDirectory(oDirBase + s"attribute_size_$combinationSize")
      info(s"Processing combinations with size: $combinationSize/$maxCombinationSize")
      val attributeIndexSeq = Star.fieldIndexSeq.combinations(combinationSize).toArray
      val maxCombination = attributeIndexSeq.length
      for(k<-0 until  maxCombination) {
          info(s"\tProcessing combination: ${k+1}/$maxCombination from combination size: $combinationSize")
          val distanceMeter = MyEuclideanDistance(attributeIndexSeq(k).toArray)
          Star.getDistance(inputCsvFile, oDir, distanceMeter, outputAttributeIndex, maxResultDistanceCount)
      }
    }
  }
  //---------------------------------------------------------------------------
  def getDistanceSimpleCase() = {
    val inputCsvFile = "/home/rafa/Downloads/work_on_users/matilde/boom/simple.txt"
    val oDirBase = Path.resetDirectory("/home/rafa/Downloads/work_on_users/matilde/boom/distance_simple")
    val attributeIndexSeq = Array(1) //regarding of class variable index
    val distanceMeter = MyEuclideanDistance(attributeIndexSeq)
    val outputAttributeIndex = 3
    val maxResultDistanceCount = 10


    Star.getDistance(inputCsvFile,oDirBase,distanceMeter,outputAttributeIndex,maxResultDistanceCount)
  }
  //---------------------------------------------------------------------------
  def getDistanceR_12() = {
    val inputCsvFile = "/home/rafa/Downloads/work_on_users/matilde/boom/boom_normalized.txt"
    val oDirBase = Path.resetDirectory("/home/rafa/Downloads/work_on_users/matilde/boom/distance_r_12")
    val distanceMeter = MyEuclideanDistance(Star.fieldIndexSeq.toArray)
    val outputAttributeIndex = 3
    val maxResultDistanceCount = 10

    Star.getDistance(inputCsvFile, oDirBase, distanceMeter,outputAttributeIndex,maxResultDistanceCount)
  }
  //---------------------------------------------------------------------------
  def mainTest(boom: Boom): Unit = {

    //Star.normalizeFile("/home/rafa/Downloads/work_on_users/matilde/boom/sample1_Landolt_584_stdstars.txt"
    //  , "/home/rafa/Downloads/work_on_users/matilde/boom/boom_normalized.txt")

    //getDistance()
    //getDistanceSimpleCase()
    getDistanceR_12()

  }
//---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file SandBox.scala
//=============================================================================


