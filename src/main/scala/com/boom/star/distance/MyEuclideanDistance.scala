/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  22/Sep/2022
 * Time:  10h:26m
 * Description: None
 */
//=============================================================================
package com.boom.star.distance
//=============================================================================
import com.boom.star.Star
import org.apache.commons.math3.ml.distance.EuclideanDistance
//=============================================================================
//=============================================================================
object MyEuclideanDistance {
  //---------------------------------------------------------------------------
  private val euclideanDistance = new EuclideanDistance()
  //---------------------------------------------------------------------------
}
//=============================================================================
import MyEuclideanDistance._
case class MyEuclideanDistance(attributeIndexSeq: Array[Int]) extends DistanceMeterTrait {
  //---------------------------------------------------------------------------
  val name =  "euclidean"
  //---------------------------------------------------------------------------
  def calculate(a: Star, b: Star)  = {
    val aValueSeq = a.getAttributeValue(attributeIndexSeq) map (_.toDouble)
    val bValueSeq = b.getAttributeValue(attributeIndexSeq) map (_.toDouble)
    euclideanDistance.compute(aValueSeq, bValueSeq)
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file MyEuclideanDistance.scala
//=============================================================================
