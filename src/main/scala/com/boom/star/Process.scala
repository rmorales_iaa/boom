/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  22/Sep/2022
 * Time:  10h:08m
 * Description: None
 */
//=============================================================================
package com.boom.star
//=============================================================================
import com.boom.star.distance.DistanceMeterTrait
import com.common.util.parallelTask.ParallelTask
import com.common.hardware.cpu.CPU

import java.util.concurrent.ConcurrentHashMap
import scala.collection.JavaConverters.mapAsScalaConcurrentMapConverter
import scala.collection.mutable.ArrayBuffer
//=============================================================================
import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
object Process {
  //---------------------------------------------------------------------------
  def run(starSeq: Array[Star]
          , distanceMeter: DistanceMeterTrait
          , maxResultDistanceCount: Int
         ) = {
    //-------------------------------------------------------------------------
    val maxIndex = starSeq.length - 1
    val map = new ConcurrentHashMap[String, ArrayBuffer[(String, Double)]]()
    //-------------------------------------------------------------------------
    case class Process(seq: Array[Int]) extends ParallelTask[Int](
      seq
      , threadCount = CPU.getCoreCount()
      , isItemProcessingThreadSafe = true
      , randomStartMaxMsWait = 100
      , verbose = false) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(startingIndex: Int) =
        Try {
          val baseStar = starSeq(startingIndex)
          val baseStarName = baseStar.Name
          map.put(baseStarName, ArrayBuffer[(String, Double)]())
          for (i <- 0 to maxIndex) {
            val currentStar = starSeq(i)
            if (baseStar != currentStar) {

              //calculate distance to the new star
              val d = distanceMeter.calculate(baseStar, currentStar)

              //add to results, sort by distance and take the first 'maxResultDistanceCount'
              val resultSeq = map.get(baseStarName)
              resultSeq += ((currentStar.Name, d))
              val sortedDistance = resultSeq.sortWith(_._2 < _._2)
              resultSeq.clear()
              resultSeq ++= sortedDistance.take(maxResultDistanceCount)
            }
          }
        }
        match {
          case Success(_) =>
          case Failure(e) =>
            error(e.getMessage + s" Error processing queue with starting index: $startingIndex")
            error(e.getStackTrace.mkString("\n"))
        }
      //-----------------------------------------------------------------------
    }
    //-------------------------------------------------------------------------
    Process(Array.range(0,starSeq.length))
    map.asScala
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Process.scala
//=============================================================================
