/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  23/Sep/2022
 * Time:  14h:41m
 * Description: None
 */
//=============================================================================
package com.boom.star
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
//=============================================================================
//=============================================================================
object GnuPlotScript {
  //---------------------------------------------------------------------------
  private val colorPalette = Array(
    "web-green"
    , "black"
    , "blue"
    , "black"
    , "pink"
    , "yellow"
    , "dark-spring-green"
    , "gray40"
    , "salmon")
  //---------------------------------------------------------------------------
  def getCurveDefinition(i: Int, indexA:Int, indexB:Int):String =  {
    val color = colorPalette(i%colorPalette.length)
    s"""my_data_file using $indexA:$indexB with points pointtype my_point_type lc rgb "$color" pointsize my_point_size notitle"""
  }
  //---------------------------------------------------------------------------
  def getCurveDefinition(maxAttributeCount: Int):String =  {
    var startIndex = 3
    (for (i <- 0 until maxAttributeCount) yield {
      val r = getCurveDefinition(i, startIndex, startIndex + 1)
      startIndex += 3
      r
    }).toArray.mkString(
      """, \
        |         """.stripMargin)
  }
  //---------------------------------------------------------------------------
  def generate(fileName: String, csvDataFilename: String, maxAttributeCount: Int, attributeName:String, attributeCount: Int) {
    val s =
      s"""
    #star of gnu script
    #--------------------------------------
    #data file separator
    set datafile separator "\\t"
    #--------------------------------------
    #variables
    my_title = 'Attribute $attributeName / Distance'

    my_x_axis_label = 'Attribute: $attributeName'
    my_y_axis_label = 'R$attributeCount distance to reference star'
    #--------------------------------------
    my_data_file='$csvDataFilename'
    #--------------------------------------
    #fonts
    my_axis_ticks_font = "Courier-New,11"
    my_axis_title_font = "Times-Roman,11"
    #--------------------------------------
    #colors

    #background
    my_plot_background_color = "#e5ecf6"

    #--------------------------------------
    #title
    set title font ",14"
    set title my_title
    #--------------------------------------
    #title
    my_point_type = 12
    my_point_size = 1
    #--------------------------------------
    #plot background color
    set object 1 rectangle from screen 0,0 to screen 1,1 fillcolor rgb my_plot_background_color behind
    #--------------------------------------
    set autoscale                        # scale axes automatically
    unset log                            # remove any log-scaling
    set grid
    #--------------------------------------
    #axis
    set xtics font my_axis_ticks_font
    set ytics font my_axis_ticks_font

    set xtic auto
    set ytic auto

    set xlabel my_x_axis_label font my_axis_title_font
    set ylabel my_y_axis_label font my_axis_title_font
    #--------------------------------------
    plot ${getCurveDefinition(maxAttributeCount)}
    #--------------------------------------
    pause -1

    #--------------------------------------
    #end of gnu script""".stripMargin

    val bw = new BufferedWriter(new FileWriter(new File(fileName)))
    bw.write(s)
    bw.close()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file GnuPlotScript.scala
//=============================================================================
