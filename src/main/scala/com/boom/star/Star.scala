/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  21/Sep/2022
 * Time:  18h:53m
 * Description: None
 */
//=============================================================================
package com.boom.star
//=============================================================================
import com.boom.star.distance.{DistanceMeterTrait}
import com.common.logger.VisualLazyLogger
import com.common.util.path.Path
import org.apache.commons.csv.{CSVFormat, CSVParser}
import java.io.{BufferedReader, FileReader}
import scala.collection.JavaConverters.asScalaBufferConverter
import scala.reflect.runtime.universe.typeOf
//=============================================================================
import java.io.{BufferedWriter, File, FileWriter}
import scala.io.Source
//=============================================================================
//=============================================================================
object Star extends VisualLazyLogger {
  //---------------------------------------------------------------------------
  private final val sep = "\t"
  //---------------------------------------------------------------------------
  val fieldIndexSeq = List(1,3,5,7,9,11,13,15,17,19,21,23)
  //---------------------------------------------------------------------------
  //https://stackoverflow.com/questions/2658060/how-to-access-a-fields-value-via-reflection-scala-2-8
  private val VARIABLE_CLASS_AND_METHOD = typeOf[Star].members
    .filter(!_.isMethod)
    .map(x => (x.name.toString, classOf[Star].getDeclaredMethod(x.name.toString.trim)))
  //---------------------------------------------------------------------------
  private def getColNameSeq(prefix: String = "") =
    classOf[Star].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = prefix + f.getName
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  private val colNameSeq = getColNameSeq()
  //---------------------------------------------------------------------------
  private val csvInputFormat = CSVFormat.DEFAULT
    .withDelimiter(' ')
    .withHeader(colNameSeq:_*)
    .withSkipHeaderRecord()
    .withIgnoreEmptyLines(true)
  //---------------------------------------------------------------------------
  def normalizeFile(inputFileName:String, outputFileName:String) = {

    val bw = new BufferedWriter(new FileWriter(new File(outputFileName)))
    val bufferedSource = Source.fromFile(inputFileName)
    for (line <- bufferedSource.getLines) {
      bw.write(line
        .trim
        .replaceAll("\t +", " ")
        .replaceAll(" +", " ") + "\n")
    }
    bufferedSource.close
    bw.close()
  }
  //---------------------------------------------------------------------------
  def loadDataset(csvFileName:String) = {

    val br = new BufferedReader(new FileReader(new File(csvFileName)))
    val starSeq = new CSVParser(br, csvInputFormat).getRecords.asScala.map { r =>
      Star(r.get(0)
        , r.get(1).toDouble
        , r.get(2).toDouble
        , r.get(3).toDouble
        , r.get(4).toDouble
        , r.get(5).toDouble
        , r.get(6).toDouble
        , r.get(7).toDouble
        , r.get(8).toDouble
        , r.get(9).toDouble
        , r.get(10).toDouble
        , r.get(11).toDouble
        , r.get(12).toDouble
        , r.get(13).toDouble
        , r.get(14).toDouble
        , r.get(15).toDouble
        , r.get(16).toDouble
        , r.get(17).toDouble
        , r.get(18).toDouble
        , r.get(19).toDouble
        , r.get(20).toDouble
        , r.get(21).toDouble
        , r.get(22).toDouble
        , r.get(23).toDouble
        , r.get(24).toDouble
      )
    }
    br.close()
    starSeq.toArray
  }
  //---------------------------------------------------------------------------
  def writeLineContent(refStarName: String
                       , resultSeq: Seq[(String,Double)]
                       , outputAttributeIndex: Int
                       , starMap: Map[String,Star]
                       , bw: BufferedWriter) = {

    val refStar = starMap(refStarName)
    bw.write(refStarName + sep)
    bw.write(getDistanceContent(refStar,resultSeq,outputAttributeIndex))
  }
  //---------------------------------------------------------------------------
  def getDistanceHeader(maxResultDistanceCount: Int, outputAttributeName: String) = {
    s"name$sep" +
    (for(i<-0 until maxResultDistanceCount) yield {s"name_$i${sep}${outputAttributeName}_$i${sep}d_$i"}).toArray.mkString(sep) +
    "\n"
  }
  //---------------------------------------------------------------------------
  def getDistanceContent(refStar:Star
                         , resultSeq: Seq[(String, Double)]
                         , outputAttributeIndex: Int) = {
    resultSeq.map { r =>
      val minDiffStarName = r._1
      val minDiff = f"${r._2}%.6f"

      minDiffStarName + sep +
      refStar.getValue(outputAttributeIndex) + sep +
      minDiff
    }.mkString(sep) + "\n"
  }
  //---------------------------------------------------------------------------
  def getDistance(inputCsvFile:String
                  , outputDir:String
                  , distanceMeter: DistanceMeterTrait
                  , outputAttributeIndex: Int
                  , maxResultDistanceCount: Int) = {
    //load and map input stars
    val starSeq = loadDataset(inputCsvFile)
    val starMap = (starSeq map (star=> (star.Name,star))).toMap
    val outputAttributeName = colNameSeq(outputAttributeIndex)

    //sst the output filenames
    val attributeCount = distanceMeter.attributeIndexSeq.length
    val baseName = outputDir + s"${distanceMeter.name}_$attributeCount"
    val csvDataFilename = baseName + ".csv"
    val gnuScriptName = baseName + ".gnuplot"

    //calculate distance and write output
    val bw = new BufferedWriter(new FileWriter(new File(csvDataFilename)))
    bw.write(getDistanceHeader(maxResultDistanceCount,outputAttributeName))

    Process.run(starSeq, distanceMeter, maxResultDistanceCount)
      .toSeq
      .sortWith(_._1 < _._1)
      .foreach { case (starName,resultSeq) =>
        writeLineContent(starName, resultSeq, outputAttributeIndex, starMap, bw)
      }
    bw.close()

    GnuPlotScript.generate(gnuScriptName,csvDataFilename, attributeCount, outputAttributeName, attributeCount)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import Star._
case class Star(
    Name:     String  //0
  , MV:       Double  //1
  , MV_err:   Double  //2
  , B_V:      Double  //3
  , BV_err:   Double  //4
  , U_B:      Double  //5
  , UB_err:   Double  //6
  , V_R:      Double  //7
  , VR_err:   Double  //8
  , V_I:      Double  //9
  , VI_err:   Double  //10
  , V_Ks:     Double  //11
  , V_Ks_err: Double  //12
  , J_H:      Double  //13
  , J_H_err:  Double  //14
  , H_Ks:     Double  //15
  , H_Ks_err: Double  //16
  , V_W1:     Double  //17
  , V_W1_err: Double  //18
  , W1_W2:    Double  //19
  , W1_W2_err:Double  //20
  , W2_W3:    Double  //21
  , W2_W3_err:Double  //22
  , W3_W4:    Double  //23
  , W3_W4_err:Double  //24
  ) {
  //---------------------------------------------------------------------------
  def getNameTypeSeq(prefix: String = "") =
    classOf[Star].getDeclaredFields.map { f =>
      f.setAccessible(true)
      val res = (prefix + f.getName, f.getType)
      f.setAccessible(false)
      res
    }
  //---------------------------------------------------------------------------
  //https://stackoverflow.com/questions/2658060/how-to-access-a-fields-value-via-reflection-scala-2-8
  def getValueSeq() =
    (for (fieldToGetter <- VARIABLE_CLASS_AND_METHOD) yield {
      val getter = fieldToGetter._2
      getter.invoke(this).toString
    }).toArray.reverse  //in the order of variable class definition
  //---------------------------------------------------------------------------
  def getValue(index: Int) = getValueSeq()(index)
  //---------------------------------------------------------------------------
  def getAsCsvLine() = getValueSeq.mkString(sep)
  //---------------------------------------------------------------------------
  def getAttributeValue(attributeIndexSeq: Array[Int]) = {
    val valueSeq = getValueSeq
    attributeIndexSeq map (valueSeq(_))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Star.scala
//=============================================================================
