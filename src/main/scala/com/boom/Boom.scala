/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  16/Feb/2020
 * Time:  00h:44m
 * Description: None
 */
//=============================================================================
package com.boom
//=============================================================================
import ch.qos.logback.classic.{Level, LoggerContext}
import java.time.Instant
import org.slf4j.LoggerFactory
//=============================================================================
import com.common.util.time.Time
import com.boom.commandLine.CommandLineParser
import com.common.logger.VisualLazyLogger
import com.common.util.time.Time._
//=============================================================================
//=============================================================================
object Boom {
  //---------------------------------------------------------------------------
  //timing
  val startMs = System.currentTimeMillis
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
import Boom._
case class Boom() extends App with VisualLazyLogger {
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
  private def initialActions(): Unit = {
    // set time zone to UTC
    Instant.now.atZone(zoneID_UTC)

    //init spark
  }
  //---------------------------------------------------------------------------
  private def finalActions(startMs:Long): Unit = {
    warning(s"Moose2 elapsed time: ${Time.getFormattedElapseTimeFromStart(startMs)}" )
  }
  //---------------------------------------------------------------------------
  private def finalActions(): Unit = {
    warning(s"Big data on our m stars: ${System.currentTimeMillis - startMs} ms " )
  }
  //---------------------------------------------------------------------------
  def commadQuery(cl: CommandLineParser): Boolean = {

    true
  }
  //---------------------------------------------------------------------------
  def commandManager(cl: CommandLineParser) : Boolean = {
    //astrometry
    if (cl.query.isDefined)                   return commadQuery(cl)
    true
  }
  //---------------------------------------------------------------------------
  def run(cl: CommandLineParser): Unit = {
    val startMs = System.currentTimeMillis

    initialActions()
    //commandManager(cl)
    SandBox.mainTest(this)

    finalActions(startMs)
  }
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Boom.scala
//=============================================================================
