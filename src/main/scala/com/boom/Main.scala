/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Feb/2020
 * Time:  15h:19m
 * Description: None
 */
//=============================================================================
package com.boom
//=============================================================================
import com.boom.commandLine.CommandLineParser
import com.common.configuration.MyConf
import com.common.logger.VisualLazyLogger
import com.common.util.path.Path
//=============================================================================
//=============================================================================
object Main extends VisualLazyLogger {
  //-------------------------------------------------------------------------
  //Version
  private final val MAJOR_VERSION = 0
  private final val MINOR_VERSION = 0
  private final val COMPILATION_VERSION = 1
  private final val DATE_VERSION = "22 Sept 2022"
  final val VERSION = s"$MAJOR_VERSION.$MINOR_VERSION.$COMPILATION_VERSION"
  final val VERSION_AND_DATE = s"$VERSION ($DATE_VERSION)"
  final val AUTHOR="Rafael Morales (rmorales [at] iaa [dot] com)"
  final val FILIATION="UDIT. IAA-CSIC. 2022"
  final val LICENSE="http://www.apache.org/licenses/LICENSE-2.0.txt"
  //---------------------------------------------------------------------------
  def main(args: Array[String]): Unit = {
    //-------------------------------------------------------------------------

    //init output directroy
    Path.ensureDirectoryExist("output/")

    val userArgs = if (args.isEmpty) Array("--help") else args
    val cl = new CommandLineParser(userArgs)

    //no command line
    MyConf.c = MyConf("input/configuration/main.conf")
    if (MyConf.c == null || !MyConf.c.isLoaded ) fatal(s"Error. Error parsing configuration file")
    else{
      info(s"----------------------------- Big data on our m stars $VERSION starts -----------------------------")
      Boom().run(cl)
      info(s"----------------------------- Big data on our m stars $VERSION ends -------------------------------")
      System.exit(0)
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Main.scala
//=============================================================================
